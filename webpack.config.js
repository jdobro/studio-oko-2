var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');

var plugins = [
  new webpack.optimize.CommonsChunkPlugin({names: ['vendor', 'manifest']}),
  // new webpack.DefinePlugin({'process.env': {NODE_ENV: JSON.stringify('production')}}),
  // new UglifyJSPlugin(),
  new HtmlWebpackPlugin({template: 'src/200.html', filename: '200.html'})
];

const VENDOR_LIBS = [
  'react', 'react-dom', 'react-router', 'react-router-dom', 'axios',
  'react-responsive', 'react-transition-group', 'react-gsap-parallax', 'gsap'
];

module.exports = {
  entry: {
    bundle: './src/index.js',
    vendor: VENDOR_LIBS
  },
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: '[name].js'
    // filename: '[name].[chunkhash].js',
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        exclude: /node_modules/,
        use: [{
          loader: 'url-loader',
          options: { limit: 40000 }
        },
        {
          loader: 'image-webpack-loader',
          options: {}
        }
        ]
      }
    ]
  },
  plugins: plugins,
  devServer: {
    historyApiFallback: { index: '200.html' },
    contentBase: 'dist'
  }
};
