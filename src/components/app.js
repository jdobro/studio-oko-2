import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import MediaQuery from 'react-responsive';
import scrollTo from 'scroll-to-element';

import Landing from './desktop/landing.desktop';

import HomeMobile from './mobile/home.mobile';
import MitchMobile from './mobile/mitch.mobile';
import KitajMobile from './mobile/kitaj.mobile';
import AutomatonMobile from './mobile/automaton.mobile';

import HomeDesktop from './desktop/home2.desktop';
import MitchDesktop from './desktop/mitch.desktop';
import KitajDesktop from './desktop/kitaj.desktop';
import AutomatonDesktop from './desktop/automaton.desktop';

import logo from '../../assets/logo.svg';
import whiteArrow from '../../assets/white-arrow.svg';
import pinkArrow from '../../assets/pink-arrow.svg';

class App extends Component {
  constructor() {
    super();
    this.state = { showLanding: true, arrowHover: false };
    this.arrowColor = this.arrowColor.bind(this);
    this.showLanding = this.showLanding.bind(this);
  }
  arrowColor() {
    const arrow = this.state.arrowHover ? pinkArrow : whiteArrow;
    return arrow;
  }
  showLanding(toShow) {
    if (this.state.showLanding !== toShow) {
      this.setState({ showLanding: toShow });
    }
  }
  landing() {
    if (this.state.showLanding) {
      return (
        <MediaQuery minWidth={1000}>
          <Landing />
        </MediaQuery>
      )
    }
    else return null;
  }
  render() {
    return (

          <div>
            {this.landing()}
            <BrowserRouter>
            <div className="app">
              <MediaQuery minWidth={1400}><div className="side yellow"></div></MediaQuery>
              <MediaQuery maxWidth={999}>
                <Route exact path='/' component={() => <HomeMobile showLanding={this.showLanding}/>} />
                <Route path='/mitch-dobrowner' component={() => <MitchMobile showLanding={this.showLanding}/>} />
                <Route path='/rb-kitaj' component={() => <KitajMobile showLanding={this.showLanding}/>} />
                <Route path='/automaton' component={() => <AutomatonMobile showLanding={this.showLanding}/>} />
              </MediaQuery>
              <MediaQuery minWidth={1000}>
                <Route exact path='/' component={() => <HomeDesktop showLanding={this.showLanding}/>} />
                <Route path='/mitch-dobrowner' component={() => <MitchDesktop showLanding={this.showLanding}/>} />
                <Route path='/rb-kitaj' component={() => <KitajDesktop showLanding={this.showLanding}/>} />
                <Route path='/automaton' component={() => <AutomatonDesktop showLanding={this.showLanding}/>} />
              </MediaQuery>

              <MediaQuery minWidth={1400}><div className="side black"></div></MediaQuery>
            </div>
            </BrowserRouter>
          </div>

    )
  }
}

export default App;
