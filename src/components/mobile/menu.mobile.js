import React, { Component } from 'react';
import scrollTo from 'scroll-to-element';

class MenuMobile extends Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(element) {
    scrollTo(element, {
    offset: 0,
    ease: 'in-out-quad',
    duration: 500
    });
  }
  render() {
    return (
        <div className="menu white">
          <ul>
            <li onClick={() => this.handleClick('.about')}>ABOUT</li>
            <li onClick={() => this.handleClick('.case-studies')}>CASE STUDIES</li>
            <li onClick={() => this.handleClick('.experiments')}>EXPERIMENTS</li>
            <li onClick={() => this.handleClick('.contact')}>CONTACT</li>
          </ul>
          <div className="anchor">
            <div className="choose">
              <h2>CHOOSE</h2>
              <h2>or scroll</h2>
            </div>
          </div>
        </div>
    )
  }
}

export default MenuMobile;
