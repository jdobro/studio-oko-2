import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FadeIn from '../fade-in';
import shiprock from '../../../assets/mitch/shiprock.jpg';
import mitchMacs from '../../../assets/mitch/mitch-macs-01.png';
import landing from '../../../assets/mitch/landing.jpg';
import menu from '../../../assets/mitch/menu.jpg';
import search from '../../../assets/mitch/search.jpg';
import exhibitions from '../../../assets/mitch/exhibitions.jpg';
import biography from '../../../assets/mitch/biography.jpg';
import civilization from '../../../assets/mitch/civilization.jpg';
import ropeOut from '../../../assets/mitch/rope-out.jpg';

class MitchMobile extends Component {
  constructor() {
    super();
  }
  componentWillMount() {
    this.props.showLanding(false);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
        <div className="case-study black-2">
          <Link to="/" className="back-X"><h3>X</h3></Link>
          <div className="white-text padding">

            <div className="header">
              <FadeIn>
                <img className="img-top" src={shiprock} />
              </FadeIn>

              <div className="name-box">
                <h4>mitch dobrowner</h4>
              </div>
            </div>

            <h5>Mitch Dobrowner is a fine-art photographer known for his striking black-and-white storm and landscape imagery.</h5>
            <br/><br/>
            <h5>We designed and built Mitch a website with a custom Content Management System.</h5>

            <FadeIn>
              <div className="two-imgs mitch-two-imgs">
                <div className="img-container flex-end"><img src={ropeOut} /></div>
                <div className="spacer-div"></div>
                <div className="img-container"><img src={moonrise} /></div>
              </div>
            </FadeIn>

            <h5>We’re really big fans of Mitch’s work. Needless to say we were thrilled to help him with his online presence. Mitch was in need of a new site to showcase his photography to fans, buyers, galleries, and museums.</h5>
          </div>

          <div className="white-triangle-up"></div>
          <div className="white padding">

            <img src={mitchMacs} style={{paddingTop: "30px"}} />

            <p className="screen-up-padding"><br/><br/><span>To get things started, we talked with Mitch</span> about what he liked about his existing site and what wasn’t working. He expressed how he wanted to keep his site as simple as possible to keep the focus on his images. At the same time, he wanted visitors to have lots of navigation options and be able to quickly find specific images. Another one of his priorities was having a really smooth site experience so visitors could view his photography without being interrupted by glitchy code or confusing design. On the backend, Mitch needed to be able to regularly update his images and exhibitions information with a Content Management System. </p>

          </div>
          <div className="white-triangle-down"></div>

          <div className="padding white-text z2">
            <div className="screen-up"><img src={landing}/></div>
            <h5>Our goal was to provide helpful features to site visitors while maintaining that sweet simplicity that makes for a great viewing experience.</h5>
            <div className="screen-down"><img src={menu}/></div>
          </div>

          <div className="white-triangle-left"></div>
          <div className="white padding">
            <p className="screen-down-padding screen-up-padding"><span>We knew we needed to build a site that looked almost bare,</span> but that would react to user needs. We decided to create a hidden menu that would slide into view when the user hovered on the left side of the screen. Everything a user would need would live on that sidebar menu– while remaining out of view when not in use. We grouped the images into five categories– recent, storms, landscapes, urban, and trees. To help visitors sift through hundreds of photographs, we included thumbnails below the main image that would facilitate an easy search. We also built a search function so people could look for images by name, location, or year.</p>
          </div>
          <div className="white-triangle-right"></div>

          <div className="white-text padding z2">
            <div className="screen-up-desktop"><img src={search}/></div>
            <h5>With each step of the design process, we asked ourselves– is this absolutely necessary? What are the design costs and gains? Is this the simplest solution?</h5>
            <div className="screen-down"><img src={biography}/></div>
          </div>

          <div className="white-triangle-up"></div>

          <div className="white padding">
            <p className="screen-up-padding screen-down-padding"><span>We built the site with our favorite tools– </span>React, Redux, and Contentful. By keeping our code fresh and clean we were able to ensure a really smooth user experience with some pretty cool features. We built Mitch a custom Content Management System so he could update his photography, tombstones, exhibitions, and other data without going into the code.</p>
          </div>

          <div className="white-triangle-down"></div>

          <div className="white-text padding z2">
            <div className="screen-up"><img src={exhibitions}/></div>
            <Link to="/"><h5 style={{textAlign: "center", padding: "60px 0"}}>take me home</h5></Link>
          </div>
        </div>
    )
  }
}

export default MitchMobile;
