import React, { Component } from 'react';
import scrollTo from 'scroll-to-element';
import logo from '../../../assets/logo.svg';
import whiteArrow from '../../../assets/white-arrow.svg';
import pinkArrow from '../../../assets/pink-arrow.svg';
import MenuMobile from './menu.mobile';
import Contact from './contact.mobile';
import CaseStudies from './case-studies.mobile';
import About1Mobile from './about1.mobile';
import About2Mobile from './about2.mobile';
import Experiments from './experiments.mobile';

class HomeMobile extends Component {
  constructor() {
    super();
  }
  componentWillMount() {
    this.props.showLanding(true);
  }
  render() {
    return (
        <div className="home">
          <div className="landing">
            <div className="split-screen yellow">
              <img src={logo} />
            </div>
            <div className="split-screen black">
              <h1>Thoughtful Design.<br />Fresh Technologies.</h1>
            </div>
          </div>
          <Spacer toElement=".menu"/>
          <MenuMobile />
          <Spacer toElement=".about"/>
          <About1Mobile />
          <About2Mobile />
          <Spacer toElement=".case-studies"/>
          <CaseStudies />
          <Spacer toElement=".experiments"/>
          <Experiments />
          <Spacer toElement=".contact"/>
          <Contact />
        </div>
    )
  }
}

class Spacer extends Component {
  constructor() {
    super();
    this.state = { arrowHover: false };
    this.arrowColor = this.arrowColor.bind(this);
  }
  arrowColor() {
    const arrow = this.state.arrowHover ? pinkArrow : whiteArrow;
    return arrow;
  }
  render () {
    return (
      <div className="spacer yellow"
        onClick={() => scrollTo(this.props.toElement, { offset: 0, ease: 'in-out-quad', duration: 500 })}
        onMouseEnter={() => this.setState({ arrowHover: true })}
        onMouseLeave={() => this.setState({ arrowHover: false })}>
        <img src={this.arrowColor()} />
      </div>
    )
  }
}

export default HomeMobile;
