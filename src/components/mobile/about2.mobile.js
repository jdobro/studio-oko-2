import React, { Component } from 'react';


function About2Mobile() {
  return (
    <div className="about2 full-page white">
      <div>
        <h2>SERVICES</h2>
        <p>ui/ux design<br />   web development<br />
        app development<br />   custom cms<br />   copywriting</p>
      </div>
      <div className="tech">
        <h2>TECHNOLOGIES</h2>
        <p>react/redux<br />   contentful<br />   mongodb<br />   node.js<br />   react native<br />   aws/google cloud</p>
      </div>
    </div>
  )
}


export default About2Mobile;
