import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import shiprock from '../../../assets/mitch/shiprock.jpg';
import londonByNight from '../../../assets/kitaj/LondonByNight.jpg';

class CaseStudiesMobile extends Component {
  constructor() {
    super();
  }
  render() {
    return (
        <div className="case-studies black white-text">

          <div className="mitch-kitaj-container">

            <Link to="/mitch-dobrowner">
              <div className="mitch">
                <img className="img-top" src={shiprock} />
                <div className="name-box">
                  <h4>mitch dobrowner</h4>
                </div>
              </div>
            </Link>

            <Link to="/rb-kitaj">
              <div className="kitaj">
                <img className="img-bottom" src={londonByNight} />
                <div className="name-box">
                  <h4>r.b. kitaj</h4>
                </div>
              </div>
            </Link>

          </div>

          <div className="anchor">
            <div className="title rotated"><h2>CASE STUDIES</h2></div>
          </div>
        </div>
    )
  }
}

export default CaseStudiesMobile;
