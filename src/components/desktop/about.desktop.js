import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class AboutDesktop extends Component {
  constructor() {
    super();
    this.getUprightAngle = this.getUprightAngle.bind(this);
    this.getUpsideDownAngle = this.getUpsideDownAngle.bind(this);
  }
  getUprightAngle() {
    const width = Math.min(window.innerWidth, 1200);
    const hypot = Math.sqrt( width*width + 200*200 );
    const rad = Math.acos( width / hypot );
    const angle = 180 + rad*180/Math.PI;
    return { transform: `rotate(${angle}deg)` };
  }
  getUpsideDownAngle() {
    const width = Math.min(window.innerWidth, 1200);
    const hypot = Math.sqrt( width*width + 200*200 );
    const rad = Math.acos( width / hypot );
    const angle = rad*180/Math.PI;
    return { transform: `rotate(${angle}deg)` };
  }
  render() {
    return (
        <div className="about black">
          <div className="header">
              <div className="triangle"></div>
              <h2 className="upright" style={this.getUprightAngle()}>ABOUT</h2>
              <h2 className="upside-down" style={this.getUpsideDownAngle()}>ABOUT</h2>
          </div>
          <div className="about-top">
            <p><span>We are a pocket-sized web design and development studio</span> in Los Angeles specializing in crafting custom sites for artists, businesses, and non-profits seeking a distinctive web presence.</p>

            <p>No matter the project, our approach is simple– combine imagination, problem solving, and a meticulous eye for a virtual experience that won’t disappoint.</p>
          </div>
          <div className="about-bottom">
            <div className="flex">
              <div><h2>SERVICES</h2></div>
              <div className="list">
                <p>ui/ux design</p><p>web development</p>  <p>app development</p>  <p>custom cms</p>  <p>copywriting</p>
              </div>
            </div>
            <div className="flex">
              <div><h2>TECHNOLOGIES</h2></div>
              <div className="list">
                <p>react/redux</p>  <p>contentful</p>  <p>mongodb</p> <p>node.js</p>  <p>react native</p>  <p>aws/google cloud</p>
              </div>
            </div>
          </div>
        </div>
    )
  }
}

export default AboutDesktop;
