import React, { Component } from 'react';
import scrollTo from 'scroll-to-element';
import logo from '../../../assets/logo.svg';
import whiteArrow from '../../../assets/white-arrow.svg';
import pinkArrow from '../../../assets/pink-arrow.svg';

class Landing extends Component {
  constructor() {
    super();
    this.state = { arrowHover: false };
    this.arrowColor = this.arrowColor.bind(this);
  }
  arrowColor() {
    const arrow = this.state.arrowHover ? pinkArrow : whiteArrow;
    return arrow;
  }
  render() {
    return (
      <div className="landing flex">
        <div className="split-screen yellow">
          <img src={logo} />
        </div>
        <div className="split-screen black">
          <h1>Thoughtful Design.<br />Fresh Technologies.</h1>
        </div>
        <div className="arrow-desktop"
          onClick={() => scrollTo('.about-anchor', { offset: 0, ease: 'linear', duration: 500 })}
          onMouseEnter={() => this.setState({ arrowHover: true })}
          onMouseLeave={() => this.setState({ arrowHover: false })}>
          <img src={this.arrowColor()} />
        </div>
      </div>
    )
  }
}

export default Landing;
