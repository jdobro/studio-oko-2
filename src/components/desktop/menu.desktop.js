import React, { Component } from 'react';
import scrollTo from 'scroll-to-element';
import blackArrow from '../../../assets/black-arrow.svg';
import pinkArrow from '../../../assets/pink-arrow.svg';

class MenuDesktop extends Component {
  constructor() {
    super();
    this.state = { arrowHover: false };
    this.arrowColor = this.arrowColor.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(element) {
    scrollTo(element, {
    offset: 0,
    ease: 'in-out-quad',
    duration: 500
    });
  }
  arrowColor() {
    const arrow = this.state.arrowHover ? pinkArrow : blackArrow;
    return arrow;
  }
  render() {
    return (
        <div className="menu white">
          <ul>
            <div className="flex">
            <li onClick={() => this.handleClick('.about')}>ABOUT</li>
            <li onClick={() => this.handleClick('.case-studies')}>CASE STUDIES</li>
            </div>
            <div className="flex">
            <li onClick={() => this.handleClick('.experiments')}>EXPERIMENTS</li>
            <li onClick={() => this.handleClick('.contact')}>CONTACT</li>
            </div>
          </ul>
          <div className="anchor">
            <div className="choose">
              <h2>CHOOSE</h2>
              <h2>or scroll</h2>
            </div>
          </div>
          <div className="arrow-desktop"
            onClick={() => scrollTo('.about', { offset: 0, ease: 'in-out-quad', duration: 500 })}
            onMouseEnter={() => this.setState({ arrowHover: true })}
            onMouseLeave={() => this.setState({ arrowHover: false })}>
            <img src={this.arrowColor()} />
          </div>
        </div>
    )
  }
}

export default MenuDesktop;
