import React, { Component } from 'react';
import scrollTo from 'scroll-to-element';
import logo from '../../../assets/logo.svg';
import whiteArrow from '../../../assets/white-arrow.svg';
import Menu from './menu.desktop';
import Contact from './contact.desktop';
import CaseStudies from './case-studies.desktop';
import About from './about.desktop';
import Experiments from './experiments.desktop';


class HomeDesktop extends Component {
  constructor() {
    super();
  }
  componentWillMount() {
    this.props.showLanding(true);
  }
  render() {
    return (
        <div className="home">
          <Menu />
          <About />
          <CaseStudies />
          <Experiments />
          <Contact />
        </div>
    )
  }
}


export default HomeDesktop;
